import axios from "axios";
import Contact from "@src/models/contact/Contact";

export const state = {}

export const getters = {}

export const mutations = {}

export const actions = {
  async create(context, data) {
    const response = await axios.post('api/contact', data.contact)
    return response.data.message;
  },
  async getByUser(){
    const response = await axios.get('api/contact/getByUser')
    return response.data.contacts.map( issue => new Contact(issue))
  }
}
