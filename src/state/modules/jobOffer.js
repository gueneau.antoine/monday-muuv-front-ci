import axios from "axios";
import JobOffer from "@src/models/job-offer/JobOffer";

export const state = {}

export const getters = {}

export const mutations = {}

export const actions = {
  all(){
    return axios.get('api/job_offers').then( response => {
      return response.data.jobOffers.map( jobOffer => {
        return new JobOffer( jobOffer )
      });
    });
  },
  getByStatus(context, data){
    return axios.post('api/job_offers/status', {status : data.status} ).then( response => {
      return response.data.jobOffers.map( jobOffer => {
        return new JobOffer( jobOffer )
      });
    });
  },
  find(context, data) {
    return axios.get('api/job_offers/' + data.id ).then( response => {
      return new JobOffer( response.data.jobOffer )
    });
  },
  create(context, data) {
    return axios.post('api/job_offers', data.jobOffer).then( response => {
      return response.data.message
    })
  },
  update(context, data) {
    return axios.put('api/job_offers/' + data.jobOffer.id , data.jobOffer).then( response => {
      return response.data.message
    })
  },
  async getJobOffersCandidate(context, data) {
    const response = await axios.get('api/job_offers/job_offers_candidate/' + data.candidate.id)
    return response.data.jobOffers.map( jobOffer => {
      return new JobOffer( jobOffer )
    });
  },
  async linkJobOffertToCandidate(context, data) {
    await axios.post(`api/job_offers/link_job_offers/${data.candidate_id}/${data.job_offer_id}`)
  },
}
