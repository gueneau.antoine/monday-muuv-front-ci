import axios from "axios";
import Meeting from "@src/models/event/Meeting";
import Setting from "@src/models/user/Setting";
import Certification from "@src/models/user/candidate/Certification";

export const state = {}

export const getters = {}

export const mutations = {}

export const actions = {
  async userSettings(context, data) {
    const response = await axios.get('api/settings')
    return response.data.settings.map(certification => new Setting({ id: certification.id, key: certification.key }))
  },
  async allSettings(context, data) {
    const response = await axios.get('api/allSettings')
    return response.data.settings.map(certification => new Setting({ id: certification.id, key: certification.key }))
  },
  async updateUserSettings(context, data) {
    const response = await axios.put('api/settings', {settings: data.settings})
    return response.data.message;
  },
}
