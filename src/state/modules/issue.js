import axios from "axios";
import Issue from "@src/models/issue/Issue";

export const state = {}

export const getters = {}

export const mutations = {}

export const actions = {
  async create(context, data) {
    const response = await axios.post('api/issue', data.issue)
    return response.data.message;
  },
  async getByUser(){
    const response = await axios.get('api/issue/getByUser')
    return response.data.issues.map( issue => new Issue(issue))
  }
}
