import User from "@src/models/user/User";
import moment from "moment";
moment.locale('fr');

export default class Message {

  content
  author
  author_id
  recipient
  recipient_id
  created_at
  updated_at

  constructor({content, author, author_id, recipient, recipient_id, created_at, updated_at}) {
    this.content = content

    if(author){
      if( author instanceof User){
        this.author = author
      }else{
        this.author = new User(author)
      }
    }

    if(recipient){
      if( recipient instanceof User){
        this.recipient = recipient
      }else{
        this.recipient = new User(recipient)
      }
    }

    this.author_id = author_id
    this.recipient_id = recipient_id
    this.created_at = created_at
    this.updated_at = updated_at
  }

  formatCreatedAt() {
    return moment( this.created_at ).fromNow();
  }

  formatUpdatedAt() {
    return moment( this.updated_at ).fromNow();
  }

}
