import moment from "moment";
moment.locale('fr');

export default class JobOffer {

  id
  company_id
  name
  required_profile
  missions
  star_date
  handicape
  horraires
  teletravail
  deplacements
  process_integration
  word_about_futur_manager
  word_about_futur_team
  salary
  show_salary
  benefits
  contract
  cdd_duration
  status
  domains
  updated_at
  created_at
  expectations
  city
  address
  active_location
  certifications

  constructor({id, company_id, name, required_profile, missions, star_date, handicape, horraires, teletravail, deplacements, process_integration, word_about_futur_manager, word_about_futur_team, contract, cdd_duration, benefits, salary, show_salary, status, domains, updated_at, created_at, city, address, certifications, active_location, expectations = []}) {
    this.id = id
    this.company_id = company_id
    this.name = name
    this.required_profile = required_profile
    this.missions = missions
    this.star_date = star_date
    this.handicape = handicape
    this.horraires = horraires
    this.teletravail = teletravail
    this.deplacements = deplacements
    this.process_integration = process_integration
    this.word_about_futur_manager = word_about_futur_manager
    this.word_about_futur_team = word_about_futur_team
    this.salary = salary
    this.show_salary = show_salary
    this.benefits = benefits
    this.contract = contract
    this.cdd_duration = cdd_duration
    this.status = status
    this.domains = domains
    this.city = city
    this.address = address
    this.active_location = active_location
    this.certifications = certifications
    this.updated_at = updated_at
    this.created_at = created_at
    this.expectations = []
    expectations.forEach( expectation => this.expectations.push( expectation.id ));
  }

  formatCreatedAt() {
    return moment( this.created_at ).fromNow();
  }

  formatUpdatedAt() {
    return moment( this.updated_at ).fromNow();
  }

}
