export default class Certification {

    id
    name
    icon
    level

    constructor({id, name, icon, pivot}) {
        this.id = id
        this.name = name
        this.icon = icon
        if(pivot){
            this.level = pivot.level
        }
    }

    getColor() {
        if(this.level === 1){
            return '#DAA520';
        }else if(this.level === 2){
            return '#8f8c8c';
        }else{
            return '#c45409';
        }
    }

}
