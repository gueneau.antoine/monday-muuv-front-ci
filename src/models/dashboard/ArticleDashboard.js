import moment from "moment";

export default class ArticleDashboard {
  id
  title
  content
  image
  date

  constructor({ id, title, content, image, date }) {
    this.id = id
    this.title = title
    this.content = content
    this.image = image
    this.date = date
  }

  formatDateFrom() {
    return moment( this.date ).fromNow();
  }

  formatDate() {
    return moment( this.date ).format('L');
  }

}
