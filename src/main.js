import Vue from 'vue'
import App from './app'
import router from '@router'
import store from '@state/store'
import BootstrapVue from 'bootstrap-vue'
import VueSweetalert2 from 'vue-sweetalert2';
import 'bootstrap/dist/js/bootstrap.js'
import axios from 'axios';
import VCalendar from 'v-calendar';

import { LMap, LTileLayer, LMarker } from 'vue2-leaflet';
import 'leaflet/dist/leaflet.css';

axios.defaults.baseURL = process.env.VUE_APP_BASE_URL;

Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-marker', LMarker);

Vue.use(BootstrapVue)
Vue.use(VueSweetalert2);

Vue.use(VCalendar, {
  firstDayOfWeek: 2,
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app')
