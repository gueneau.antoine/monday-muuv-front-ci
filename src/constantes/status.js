export default [
  {
    'text' : 'Nouveau',
    'value' : 10,
  },
  {
    'text' : 'En cours',
    'value' : 20,
  },
  {
    'text' : 'Résolue',
    'value' : 30,
  }
]
