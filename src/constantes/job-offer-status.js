export default [
  {
    'text' : 'Brouillon',
    'value' : 1,
  },
  {
    'text' : 'Publié',
    'value' : 2,
  },
  {
    'text' : 'Archivé',
    'value' : 3,
  },
]